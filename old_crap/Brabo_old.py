#!c:/Python27/python.exe -u

import Skype4Py
import string
import sqlite3
from multiprocessing import Queue

from commands import allc

def safety(string):
	return string

class Chatbot:
	def __init__(self):
		self.motions = []
		self.q = Queue()
		self.showrolls = False
		# Create an instance of the Skype class.
		skype = Skype4Py.Skype();
		skype.OnMessageStatus = self.command
		if skype.Client.IsRunning == False:
			print "Please start skype before starting the chatbot."
			raise

		# Connect the Skype object to the Skype client.
		skype.Attach();

		#And then an endless loop
		print 'ready'
		while True:
			raw_input('')
			

	def command(self, Message, Status):
		
		print(Message)
		print(Message.Timestamp)
		print(Message.Sender.Handle)
		print(Message.Status)
		
		
		bodl = Message.Body.lower()
		bod = Message.Body
		if len(bod) < 1:
			return
		# check to see if user has notes
		if allc.notes.testnotes(Message.FromHandle):
			Message.Chat.SendMessage(allc.notes.getnotes(Message.FromHandle))
			
		# update !seen status
		allc.seen.updateseen(Message)
		
		if Status == 'RECEIVED': # if Status == 'SENT' => brabo sent it
			Message.MarkAsSeen()
		
		if Status == 'READ':
		# for rat:
		#if Status == 'RECEIVED' or Status == 'SENT':
		
			# add "!seen" logging here
			# open sql to brabo.db
			# update /username/ to last seen now (~notes)
			# possibly update what /username/ was saying
			
			
			# dieroll commands
			if bod[0] == '%': 
				msg = allc.dieroll.dieroll(bod, self.showrolls)
				if msg != -1:
					Message.Chat.SendMessage(msg)

			# standard commands
			elif bod[0] == '!' or bodl[0:5] == 'brabo' or bodl[0:6] == 'brabo:':
				if bodl[:6] == 'brabo:':
					bod = bod[6:].strip()
				elif bodl[:5] == 'brabo':
					bod = bod[5:].strip()
				else:
					bod = bod[1:].strip()

				# hardcoded commands
				if bod == 'reload':
					reload(allc)
					Message.Chat.SendMessage('Reloaded.')
				else: # modular commands
					# start thread
					# old:
					#Message.Chat.SendMessage(safety(allc.cmd(self, bod, Message)))
					#new:
					allc.cmd(self, bod, Message)
					output = False
					while not output:
						try:
							#Message.Chat.SendMessage(self.q.qsize())
							Message.Chat.SendMessage(self.q.get(True, 1))
							output = True
						except:
							"do nothing"

			# specific words
			elif bodl[:8] == 'slaapwel' or bodl[:8] == 'Slaapwel':
				Message.Chat.SendMessage('Slaapwel.')
	

if __name__=="__main__":
	Brabo = Chatbot()
	print "End of file!"
