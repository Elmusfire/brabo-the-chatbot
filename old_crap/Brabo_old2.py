#!c:/Python27/python.exe -u



#import Skype4Py
import discord

import string
import sqlite3
import os
import sys
#from multiprocessing import Queue
import importlib
import random


import ConfigParser

def safety(string): #todo: string safety if / when needed
	return string
	
class Chatbot:
	def __init__(self):
		#self.motions = []
		#self.q = Queue()
		self.showrolls = False
		self.commands = {}
		self.checks = []
		self.loaded_mods = []
		# Create an instance of the Skype class.
		#skype = Skype4Py.Skype();
		#skype.OnMessageStatus = self.command
		#if skype.Client.IsRunning == False:
		#	print "Please start skype before starting the chatbot."
		#	raise

		# Connect the Skype object to the Skype client.
		#skype.Attach();

		# load commands
		self.loadmods(0, 0, 0)
		

		@client.event
		def on_message(message):
			if message.author.name != client.user.name:
				print dir(message.author)
				print message.author.avatar
				print message.author.avatar_url
				print message.author.discriminator
				print message.author.id
				print message.author.id
				print message.author.id
				print message.author.mention
				print message.author.name
				self.command(message, client)

		@client.event
		def on_ready():
			print('Logged in as')
			print(client.user.name)
			print(client.user.id)
			print('------')

		client.run()
		#while True:
		#	raw_input('')
			
	def loadmods(self, msg, timestamp, sender):
		'''(re)loads all .py files in the commands folder that don't start with '_' '''
		# get list of files
		# load files
		# add functions to commands
		# eventually print something on duplicates
		
		self.commands = { u'reload': self.loadmods,
						u'help': self.help}							
		files = []
		modcount = 0
		modrecount = 0
		overwrcount = 0

		for fil in os.listdir('commands') :
			if fil[0] != '_' and fil[-3:] == '.py':	# get all python files that don't start with _
				files.append(fil)
		print('loading these files')
		print(files)

		for fil in files:
			modname = 'commands.' + fil[:-3]
			if modname in sys.modules:				#if it's been loaded, reload, else just import it
				reload(sys.modules[modname])
				modrecount += 1
			else:
				importlib.import_module(modname)
				modcount += 1
			
			try:
				mod = sys.modules[modname]
				temp = mod.load()
				print('\nadding:')					#load() returns a dict, so get all values in there and put it in "commands"
				print(temp)
				for key in temp.keys():
					self.commands[key] = temp[key]					
			except AttributeError as e:
				#if load doesn't exist do diddly squat i spose
				print("load() doesn't exist in this module")
				print(e)

			try:					
				temp_checks = mod.load_checks()
				print("\nadding checks:")
				print(temp_checks)
				for check in temp_checks:
					self.checks.append(check)
			except AttributeError as e:
				#if load doesn't exist do diddly squat i spose
				print("load_checks() doesn't exist in this module")
				print(e)

		print('commands:\n' + str(self.commands))
		return 'reload completed.\n' + str(modcount) + ' new modules loaded.\n' + str(modrecount) + ' modules reloaded.\n' + str(overwrcount) + ' command conflicts' 

	def help(self, msg, timestamp, sender):
		''' you're already reading this, i don't think you need any more help'''
		if(msg.split(' ', 1)[0] in self.commands):			
			if self.commands[msg].__doc__ != None:
				return self.commands[msg].__doc__
			else:
				return "I don't have any documentation for '" + msg + "'"
		else:
                        ret = "'" + msg + "' is not a known command"
                        ret += "\ncurrent known commands:\n"
			for com in self.commands:
				ret += com + ", "
			return ret

	def command(self, message, client):
		print(message)
		print(message.timestamp)
		myTimestamp = message.timestamp
		print(message.author.name)
		myAuthorName = message.author.name

		bod = unicode(message.content)
		
		if len(bod) < 1:
			return ""

		for check in self.checks:
			print("testing for check")
			ret = check(bod, myTimestamp, myAuthorName)
			if ret == '' or ret == None:
				print("check no")
				continue
			else:
				print("check yes")
				client.send_message(message.channel, ret)
		
		
		# check if we're addressed
		if bod[0] == '!' or bod[0] =='%' or bod[0:5].lower() == 'brabo' or bod[0:6].lower() == 'brabo:' or bod[0:6].lower == 'brabo,':
			if (bod[:6].lower() == 'brabo:') or (bod[:6].lower() == 'brabo,'):
				bod = bod[6:].strip()
			elif bod[:5].lower() == 'brabo':
				bod = bod[5:].strip()
			else:
				bod = bod[1:].strip()
			
			# 1 get first word out of commands
			print('bod = ')
			print(bod)
			print('commands = ')
			print(self.commands)
			temp = bod.split(' ', 1)
			
			# 2 call function
			
			if temp[0] in self.commands:
				if len(temp) == 1:
					ret = self.commands[temp[0].lower()]('', myTimestamp, myAuthorName)
				else:
					ret = self.commands[temp[0].lower()](temp[1], myTimestamp, myAuthorName)
			elif bod[-1] == '?':
				client.send_message(message.channel, self.magic8())
			else:			
				return	#currently thinking about this. if you say ! with an invalid command, reply with error?				
			# 3 print return
			output = False
			client.send_message(message.channel, ret)
			'''
			while not output:
				try:
					#Message.Chat.SendMessage(self.q.qsize())
					Message.Chat.SendMessage(ret)
					output = True
				except:
					"do nothing"
			'''	
		
		# specific words, regardless of being addressed
		if bod[:8].lower() == 'slaapwel':
			client.send_message(message.channel, 'Slaapwel. :sleeping:')
		
		print "end of command"

	def magic8(self):
		'''just ask me a question bro'''
		print "duck 2"
		replies = ["It is certain","It is decidedly so","Without a doubt","Yes definitely","You may rely on it","As I see it yes","Most likely","Outlook good","Yes","Signs point to yes","Reply hazy try again","Ask again later","Better not tell you now","Cannot predict now","Concentrate and ask again","Don't count on it","My reply is no","My sources say no","Outlook not so good","Very doubtful","Ayy lmao"]
		print "duck 3"
		return random.choice(replies)

while __name__=="__main__":
	config = ConfigParser.RawConfigParser()
	config.read('config.ini')
	client = discord.Client()
	client.login(config.get('logininfo', 'email'), config.get('logininfo', 'pass'))

	Brabo = Chatbot()
	from time import sleep
	sleep(5)
	print "End of file!"
