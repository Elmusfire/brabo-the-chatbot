# coding=utf-8
import urllib
from urllib import FancyURLopener
import json
import random

def load():
	return {u'reaction': reaction}

	
def reaction(msg, timestamp, sender):
	u'''Responds with a reaction gif from http://replygif.net/, argument: one or more tags. Use _ for spaces within tags, or see all tags at http://replygif.net/t/all'''
	splitted = msg.split()
	url = "http://replygif.net/api/gifs?tag=" 
	url += splitted[0].replace("_", " ")
	for i in splitted[1:]:
		url += "," + i.replace("_", " ")
	url += "&tag-operator=and&api-key=39YAprx5Yi"
	class MyOpener(FancyURLopener):
		version = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11'
	
	#myreaction = urllib.urlopen(url)
	myopener = MyOpener()
	return random.choice(json.loads(myopener.open(url).read()))[u"file"]


def longreaction(msg, timestamp, sender):
	u'''not used, long version of what the return in reaction does'''
	splitted = msg.split()
	url = "http://replygif.net/api/gifs?tag=" 
	url += splitted[0]
	for i in splitted[1:]:
		url += "," + i
	url += "&tag-operator=and&api-key=39YAprx5Yi"
	class MyOpener(FancyURLopener):
		version = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11'
	
	#myreaction = urllib.urlopen(url)
	myopener = MyOpener()
	myreaction = myopener.open(url)
	temp = myreaction.read()
	myjson = json.loads(temp)
	choice1 = random.choice(myjson)
	return choice1[u"file"]

