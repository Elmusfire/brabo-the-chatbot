import sqlite3
from compiler.pycodegen import TRY_FINALLY
from _ast import TryExcept
from symbol import try_stmt

Motions = []

def load():
	return {u'vote': vote,
			u'motion': addVote,
			u'votes': getVotes}

class Motion:
	def __init__(self, cmd):
		self.cmd = cmd
		self.votes = {}
		# fetch all users from the DB
		conn = sqlite3.connect('brabo.db')
		c = conn.cursor()
		c.execute('''SELECT username FROM users''')
		dbout = c.fetchall()
		
		for i in range(len(dbout)):
			self.votes[dbout[i][0]] = ""
	
	#returns how many voted in favor	
	def passPerc(self):
		length = len(self.votes)
		yes = 0

		for i in self.votes.keys():
			if self.votes[i] == "y":
				yes += 1
		return int(yes / float(length) * 100)
		
	def failPerc(self):
		length = len(self.votes)
		no = 0

		for i in self.votes.keys():
			if self.votes[i] == "n":
				no += 1
		return int(no / float(length) * 100)
	
	def votePerc(self):
		length = len(self.votes)
		voted = 0
		for i in self.votes.keys():
			if not self.votes[i] == "":
				voted += 1
		return int(voted / float(length) * 100)
	
	def vote(self, name, vote):
		self.votes[name] = vote
			
	def String(self):
		ret = "\""
		ret += self.cmd + "\" "
		ret += "| voted: " + str(self.votePerc()) + "% | in favor: " + str(self.passPerc()) + "%"
		return ret

def getVotes(msg, timestamp, sender):
	'''print out the current running motions'''
	global Motions
	if len(Motions) == 0:
		return "there are no current votes in progress"
	else:
		ret = ""
		i = 0
		for mot in Motions:
			ret += "motion " + str(i) + ": "
			ret += " " + mot.String() + "\n"
			i += 1
		return ret

def addVote(msg, timestamp, sender):
	'''creates a vote \nsyntax: !motion [description]'''
	global Motions
	Motions.append(Motion(msg))
	return "added a motion to vote over: \"" + msg + "\"\n" + getVotes(brabo)

def vote(msg, timestamp, sender):
	'''vote for a motion.\nsyntax: !vote [motion number] [y or n]'''
	global Motions	
	voter = sender
	msg = msg.split()
	try:
		mot = int(msg[0])
		if (msg[1] in ["y", "yes", "aye"]):
			Motions[mot].vote(voter, "y")
		elif (msg[1] in ["n", "no", "nay"]):
			Motions[mot].vote(voter, "n")
		else:
			return "please vote aye or nay"
		
		ret = resolveMot(mot)
		if ret:
			return ret
		else:
			return "voted \"" + msg[1] + "\" for motion " + str(mot)
	except ValueError:
		return "invalid number entered, please refer to motions by their number"
  
def dropVote(msg, timestamp, sender):
	'''deletes a vote\nsyntax: !dropvote [motion number]'''
	global Motions
	del Motions[int((msg.split)[0])]  

def resolveMot(mot):
	motion = Motions[mot]
	print "\n"
	print "motions: \n"
	ret = ""
	for motion in brabo.motions:
		print "motion: "
		print "\n" + str(motion.cmd) + ": " + str(motion.votes)
		
	if motion.passPerc() > 60:
		ret = "the motion: \"" + motion.cmd + "\" has passed"
		del Motions[mot]	
	elif motion.failPerc() > 40:
		ret =  "the motion: \"" + motion.cmd + "\" has failed"
		del Motions[mot]
	elif motion.votePerc() == 100:
		if motion.passPerc() > 50:
			ret = "the motion: \"" + motion.cmd + "\" has failed"
			del Motions[mot]		
		else :
			ret = "the motion: \"" + motion.cmd + "\" has failed"
			del Motions[mot]
	return ret
