#coding: UTF-8

'''
List of commands, simple reply commands being executed on the spot, longer ones being rerouted to the relevant function

TODO:
volgende lijn roept een functie op via string
>>> locals()["foo"]()
geeft blijkbaar problemen met stuff ofzo, volgende is "beter"
>>> m = getattr(all, foo, 'default')

>>> import os
... for files in os.listdir("."): # in this directory
...     if files.endswith(".py"): # all files ending on .py
...         print files

print a function's docstring:
>>> print function.__doc__

volgende is een dictionary van functies
>>> short = {'tr' : translate}
'''

import os
commands = {}
for filename in os.listdir("."): # in this directory
	if filename.endswith(".py"): # all files ending on .py
		#commands[filename[:-3]] = 
		#
		'''lalalala'''

import administrative
import notes
import dieroll
import magic8
import seen
import translate
import roulette
import time
import fml

import allc # this is needed for help to address functions within this namespace, like test and help itself, it won't be needed for the other functions and might be removed altogether eventually

def ctest():
	''' test docstring '''
	print "hello world"

def B_help(cmdarg):
	'''
	This is the help manual on help. Usage:
	"!help <command>"
	For a list of commands, type "!commands".
	'''

	if hasattr(allc, cmdarg):
		m = getattr(allc, cmdarg)
		temp = m.__doc__
		if temp == "":
			return "No help for that command."
		else:
			return temp
	else:
		return "Specified command does not exist."

def cmd2(brabo, cmdarg, msg):
	'''docstring :D'''
	

from multiprocessing import Process, Queue
def cmd_(brabo, cmdarg, msg, b_help = False):
	f = cmdarg.split(" ", 1)[0]
	arg = cmdarg.split(" ", 1)[1]
	p = Process(target=f, args=[arg,q])
	p.start()
	p.join()


def cmd(brabo, cmdarg, msg, b_help = False):
	print "Still using cmd :("
	#sort commands by length to ensure correct parsing or split
	list = ['translate', 'showrolls', 'rollchar', 'commands', 'getnotes', 'alias', 'test', 'note', 'gmo', 'gmi', 'tr', '++', '--', "end with '?'"]
	 
	
	if cmdarg[:9] == 'translate':
		brabo.q.put(translate.translate(cmdarg[9:].strip(), b_help))
	elif cmdarg[:9] == 'showrolls':
		brabo.showrolls = not(brabo.showrolls)
		brabo.q.put('Dicerolls shown:' + str(brabo.showrolls))
	elif cmdarg[:8] == "roulette":
		brabo.q.put(roulette.roulette(cmdarg[8:].strip()))
	elif cmdarg[:8] == 'rollchar':
		brabo.q.put(dieroll.rollchar())
	elif cmdarg[:8] == 'commands':
		brabo.q.put(list)
	elif cmdarg[:8] == 'getnotes':
		brabo.q.put(notes.getnotes(msg.FromHandle))
	elif cmdarg[:7] == "addvote":
		brabo.q.put(administrative.addVote(brabo, cmdarg[8:].strip()))
	elif cmdarg[:5] == 'addme':
		brabo.q.put(notes.adduser(msg.FromHandle))
	elif cmdarg[:5] == "votes":
		brabo.q.put(administrative.getVotes(brabo))
	elif cmdarg[:5] == 'alias':
		brabo.q.put(notes.setalias(cmdarg))
	elif cmdarg[:5] == "sleep":
		brabo.q.put(time.sleep(cmdarg[5:].strip()))
	elif cmdarg[:4] == 'test':
		brabo.q.put('Test succeeded!')
	elif cmdarg[:4] == 'note':
		brabo.q.put(notes.setnote(cmdarg, msg))
	elif cmdarg[:4] == 'seen':
		brabo.q.put(seen.seen(msg))
	elif cmdarg[:4] == 'help': # TODO
		print cmdarg[4:].strip()
		brabo.q.put(B_help(cmdarg[4:].strip()))
	elif cmdarg[:4] == 'vote':
		brabo.q.put(administrative.vote(brabo, msg, cmdarg[4:]))
	elif cmdarg[:4] == 'wauw': #yolo
		brabo.q.put('https://www.youtube.com/watch?v=5sGD-wK_Brc#t=103')
	elif cmdarg[:3] == 'pls' or cmdarg[0:3] == 'plz': # fun command, unofficial, doesn't show up in the list
		brabo.q.put('STAHP')
	elif cmdarg[:3] == 'fml':
		brabo.q.put(fml.fml())
	elif cmdarg[:3] == 'gmo':
		brabo.q.put('Goede morgen.')
	elif cmdarg[:3] == 'gmi':
		brabo.q.put('Goede middag.')
	elif cmdarg[:3] == 'gmj':
		brabo.q.put('Ohayō')
	elif cmdarg[:3] == 'wat': #yolo
		brabo.q.put('https://www.youtube.com/watch?v=PDXrXBsTFSE')
	elif cmdarg[:2] == 'tr':
		brabo.q.put(translate.translate(cmdarg[2:].strip(), b_help))
	elif cmdarg[:2] == '++':
		brabo.q.put('Thanks :)')
	elif cmdarg[:2] == '--':
		brabo.q.put('Sorry :(')
	elif cmdarg[-1:] == '?':
		brabo.q.put(magic8.magic8())
	else:
		if b_help:
			brabo.q.put("Valid commands are " + str(list))
		else:
			brabo.q.put("Command not recognized. Use '!help' for help with commands")
