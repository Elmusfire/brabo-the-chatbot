# coding=utf-8
import urllib

def load():
	return {u'fml': fml}

def tldr(msg, timestamp, sender):
	u'''prints a "funny" FML. "page sucked" means the site timed out or something ¯\_(ツ)_/¯'''
	fml = urllib.urlopen("http://rscript.org/lookup.php?type=fml")
	return fml.read().split('\n')[4][6:]

