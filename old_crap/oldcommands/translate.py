#coding: utf-8

"""
This file handles everything that has to do with the translation engine goslate.
"""
import goslate

def load():
	return {u'tr': translate,
			u'translate': translate}

def translate(msg, timestamp, sender):
	'''translates the message to a language.  example: !tr ja hello 
supported languages: 'el': 'Greek', 'eo': 'Esperanto', 'en': 'English', 'zh': 'Chinese', 'af': 'Afrikaans', 'sw': 'Swahili', 'ca': 'Catalan', 'it': 'Italian', 'iw': 'Hebrew', 'cy': 'Welsh', 'ar': 'Arabic', 'ga': 'Irish', 'cs': 'Czech', 'et': 'Estonian', 'gl': 'Galician', 'id': 'Indonesian', 'es': 'Spanish', 'ru': 'Russian', 'nl': 'Dutch', 'pt': 'Portuguese', 'mt': 'Maltese', 'tr': 'Turkish', 'lt': 'Lithuanian', 'lv': 'Latvian', 'tl': 'Filipino', 'th': 'Thai', 'vi': 'Vietnamese', 'ro': 'Romanian', 'is': 'Icelandic', 'pl': 'Polish', 'yi': 'Yiddish', 'be': 'Belarusian', 'fr': 'French', 'bg': 'Bulgarian', 'uk': 'Ukrainian', 'sl': 'Slovenian', 'hr': 'Croatian', 'de': 'German', 'ht': 'Haitian Creole', 'da': 'Danish', 'fa': 'Persian', 'hi': 'Hindi', 'fi': 'Finnish', 'hu': 'Hungarian', 'ja': 'Japanese', 'zh-TW': 'Chinese (Traditional)', 'sq': 'Albanian', 'no': 'Norwegian', 'ko': 'Korean', 'sv': 'Swedish', 'mk': 'Macedonian', 'sk': 'Slovak', 'zh-CN': 'Chinese (Simplified)', 'ms': 'Malay', 'sr': 'Serbian' '''
	gs = goslate.Goslate()
	cmdsplit = msg.split(' ', 1)
	print "cmdsplit =:"
	print cmdsplit
	st = gs.translate(cmdsplit[1], cmdsplit[0])
	print "st = :"
	print st
	ret = "Translation of \"" + cmdsplit[1] + "\":\n\"" + st + "\""
	return ret.encode('utf-8')