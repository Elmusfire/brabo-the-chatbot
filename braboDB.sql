PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS hangmanScores(	
	username text CONSTRAINT hmscores_pk PRIMARY KEY, 
	score integer
);

CREATE TABLE IF NOT EXISTS hangmanGames(
	word text,
	used text,
	wrong integer,
	winner text,
	participants text,
	startdate text,
	enddate text
);

CREATE TABLE IF NOT EXISTS emojiBlacklist(
	character text PRIMARY KEY
);

/*
CREATE TABLE IF NOT EXISTS users (
	username text CONSTRAINT users_pk PRIMARY KEY,
	hasnote integer,
	karma integer
);

CREATE TABLE IF NOT EXISTS aliases (
	alias text CONSTRAINT aliases_pk PRIMARY KEY,
	user text,
	FOREIGN KEY(user) REFERENCES users(username)
);

CREATE TABLE IF NOT EXISTS notes (
	user text,
	note text,
	FOREIGN KEY(user) REFERENCES users(username)
);

CREATE TABLE IF NOT EXISTS admins (
	user text CONSTRAINT admins_pk PRIMARY KEY,
	FOREIGN KEY(user) REFERENCES users(username)
);

CREATE TABLE IF NOT EXISTS time (
	id integer CONSTRAINT time_pk PRIMARY KEY,
	tstamp integer
);

CREATE INDEX "user" ON notes (user DESC);
CREATE UNIQUE INDEX "username" ON users (username DESC);
*/
