import asyncio
import requests
import time
from discord.ext import commands
import sys

class AdminCommands:


	def __init__(self, bot):
		self.bot = bot
		#				Ratty				Fantom
		self.dev_ids = ['79884312748498944', '79622319546310656']
		self.start_time = time.time()

	@commands.command(pass_context=True, no_pm=False)
	async def test(self, ctx, msg = None):
		"""replies with what msg looks like to the bot"""
		print("test called, ctx looks like : \n" + str(dir(ctx.message)) +
				#"\nmsg2 = " + str() +
			  "\ntype:" + str(type(msg)) +
			  "\nsender:" + str(ctx.message.author.id) +
			  "\nmsg:" + str(msg) +
			  "\nctx message:" + str(ctx.message.content) +
			  "\nctx clean message:" + str(ctx.message.clean_content) +
			  "\n:"
		)
		try:
			print(msg.reactions)
		except:
			print("nvm")
			print("dir:", dir(msg))
		#for part in ctx.message.content.split(":")
		#	await self.bot.add_reaction(ctx.message, emoji)
		await self.bot.say('this looks like `' + str(ctx.message.clean_content) + '`')
		await self.bot.say('this looks like `' + str(ctx.message.content) + '`')

	@commands.command(pass_context=True, no_pm=False)
	async def testfull(self, ctx, *msg):
		"""replies with what msg looks like to the bot"""
		if msg is not None:
			await self.bot.say('this looks like `' + str(msg) + '`')
		else:
			await self.bot.say("I didn't get anything")

	@commands.command(pass_context=False, no_pm=False)
	async def uptime(self):
		"""replies with what msg looks like to the bot"""
		waketime = time.time() - self.start_time
		wakeword = "seconds"
		if waketime / 60 > 2:
			waketime =  waketime // 60
			wakeword = "minutes"
			if waketime / 60 > 2:
				waketime = waketime // 60
				wakeword = "hours"
				if waketime / 24 > 2:
					waketime = waketime // 24
					wakeword = "days"
		blurb = "" # todo: add random things like "beat that, humans" and "I could use a break" about .1% of the time
		await self.bot.say("I've been awake for " + str(int(waketime)) + " " + wakeword + blurb + ".")

	@commands.command(pass_context=True, no_pm=False)
	async def plseval(self, ctx, msg):
		"""evals the expression (admin only)"""
		if ctx.message.author.id in self.dev_ids:
			await self.bot.say("evaluating " + str(ctx.message))
			await self.bot.say(str(eval(ctx.message)))
			await self.bot.say("done")
		else:
			await self.bot.say("You're not allowed to do that!")

	@commands.command(pass_context=True, no_pm=False)
	async def ip(self, ctx):
		"""Prints IP (admin only)"""
		if ctx.message.author.id in self.dev_ids:
			await self.bot.say(requests.get('http://ip.42.pl/raw').text)
		else:
			await self.bot.say("You're not allowed to do that!")


	@commands.command(pass_context=True, no_pm=False)
	async def reboot(self, ctx):
		"""Reboots the bot (admin only)"""
		if ctx.message.author.id in self.dev_ids:
			await self.bot.say("Rebooting...")
			sys.exit(0) # lol
		else:
			await self.bot.say("You're not allowed to do that!")

