import asyncio
from discord.ext import commands
import requests

class ReplyCommands:
	def __init__(self, bot):
		self.bot = bot
		self.fmlcache = []

	@commands.command(pass_context=True, no_pm=False)
	async def gmo(self, ctx):
		"""Replies with 'Good morning :)' """
		await self.bot.say('Good morning :)')


	@commands.command(pass_context=True, no_pm=False)
	async def fml(self, ctx):
		'''prints a "funny" FML.'''
		if len(self.fmlcache) < 5:
			try:
				assert False, "There are no good FMLs"
				print("getting fresh fmls")
				url = '''http://www.fmylife.com/api/v2/article/list?page[number]=1&page[bypage]=20&status[]=4&orderby[RAND()]=ASC'''
				headers = {"X-VDM-Api-Key": "4dccf019bdfac"}
				reply = requests.get(url, headers = headers).json()
				self.fmlcache.extend( reply["data"] )
			except Exception as e:
				print(str(e))
				await self.bot.say("i'm sorry, i can't get any good FMLs right now")
				return
						
		print("posting FML")

		nextFml = self.fmlcache.pop()["content_hidden"]
		while not nextFml.startswith("Today"):
			nextFml = self.fmlcache.pop()["content_hidden"]

		await self.bot.say(nextFml)

	@commands.command(pass_context=True, no_pm=True)
	async def tldr(self, ctx, *, msg: str=None):
		'''makes a tldr of the accompanying url'''
		if msg == None:
			msg = ""
		r = requests.get('http://api.smmry.com/?SM_API_KEY=5B9E089713&SM_URL=' + msg.strip("\"'"))
		text = r.json()
		print(text)
		title = ""
		content = ""
		if 'sm_api_title' in text and text['sm_api_title'] != "":
			title = "__" + text['sm_api_title'] + "__\n"
		if 'sm_api_content' in text:
			content = text['sm_api_content']
		if 'sm_api_message' in text:
			await self.bot.say(text['sm_api_message'])
			# quit here? or just say "I got nothing." anyway?
		result = title + content
		if result != "":
			for i in range(0, len(result), 1000): # discord hates when you print more than 2000 chars at once
				await self.bot.say(result[i:i+1000]) # TODO: split on nearest "." instead
		else:
			await self.bot.say("I got nothing.")
