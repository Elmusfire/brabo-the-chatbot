import discord
from discord.ext import commands
import random
import string
import commands.database as database

class Votes: 
	def __init__(self, bot):
		self.bot = bot

	@commands.group(pass_context=True)
	async def Kappa(self, ctx):
		await self.bot.say(":Kappa:")

	#@commands.group(invoke_without_command=True, pass_context=True)
	@commands.group(pass_context=True)
	async def testunicode(self, ctx):
		await self.bot.say("adding 😁 to your message")
		await self.bot.add_reaction(ctx.message, '😁')
		await self.bot.say("added 😁 to your message")
	
	@commands.group(pass_context=True)
	async def testallemojis(self, ctx):
		await self.bot.say("adding ten emojis to your message")
		for i in random.choices(list(self.bot.get_all_emojis()), k=10):
			print(i)
			await self.bot.add_reaction(ctx.message, i)
		await self.bot.say("added ten emojis to your message")

	@commands.group(pass_context=True)
	async def vote(self, ctx):
		db = database.database()
		for i in ctx.message.content:
			if not db.isBlacklistChar(i):
				try:
					await self.bot.add_reaction(ctx.message, i)
				except Exception as e:
					print(type(e),e)
					print(f"'{i}'")
					print("why")
					db.addBlacklistChar(i)
		for i in self.bot.get_all_emojis():
			print(i)
			print(str(i))
			if str(i) in ctx.message.content:
				await self.bot.add_reaction(ctx.message, i)
