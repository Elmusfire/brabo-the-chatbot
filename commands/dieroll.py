import random
import discord
from discord.ext import commands
import re

class DierollCommands:
	def __init__(self, bot):
		self.bot = bot
		self.showrolls = {}
	
	@commands.command(description='For when you wanna settle the score some other way')
	async def choose(self, *choices : str):
		"""Chooses between multiple choices."""
		await self.bot.say(random.choice(choices))


	@commands.command(pass_context=True, no_pm=True, alias = ['TSR'])
	async def toggleShowrolls(self, ctx):
		'''toggles whether I'm showing you all the separate rolls, or just the total'''
		#print(ctx.message)
		#print(ctx.message.channel.id)
		#print(ctx.message.server.id)
		if ctx.message.channel.id not in self.showrolls:
			self.showrolls[ctx.message.channel.id] = False
		self.showrolls[ctx.message.channel.id] = not self.showrolls[ctx.message.channel.id]
		if self.showrolls[ctx.message.channel.id]:
			await self.bot.say("Individual rolls visible.")
		else:
			await self.bot.say("Individual rolls invisible.")
		
	def subroll(self, msg : str):
		# verify it has a d in it
		d = msg.find('d')
		#print "dloc: " + str(d)
		amount = 1
		if d < 0:
			# e.g. with 1d8 +5, the "+5" gets to this part
			if msg.isnumeric():
				return(msg, msg)
		if d > 0: 
			# check if there's an increased amount of dierolls
			#print "msg[:d]:" + str(msg[:d])
			if msg[:d].isnumeric():
				amount = int(msg[:d])
			else:
				return None

		# find the size of the die
		size = 0
		if msg[(d+1):].isnumeric():
			size = int(msg[(d+1):])
		else:
			return None

		print('dieroll: amount = ' + str(amount) + ', size = ' + str(size))
		ret = ''
		rolls = []
		if amount == 1:
			ret = random.randint(1, size)
			rolls.append(ret)
		elif amount > 1:
			total = 0
			for i in range(amount):
				temp = random.randint(1, size)
				total += temp
				rolls.append(temp)
			ret = str(total)
		return(ret, rolls)
		
	@commands.command(pass_context=True, no_pm=True)
	async def roll(self, ctx, *, msg : str):
		'''example: roll 3d6\nNdX+Y, rolls N dice of size X, adds Y to the result'''
		
		if ctx.message.channel.id not in self.showrolls:
			self.showrolls[ctx.message.channel.id] = False

		print("incoming msg: ")
		print(msg)
		# clean the message
		msg = msg.lower().replace(" ", "")
		#print "we entered the dieroll"
		#print msg
		
		if msg.find('=') != -1:
			await self.bot.say("Can't roll with that '=' in the way")
			return
		
		msg = msg.replace("+", "=+")
		msg = msg.replace("-", "=-")
		if msg[0] != '+':
			msg = "+" + msg
			
		print("cleaned msg:")
		print(msg)
		
		total = 0
		totalrolls = []
		for mess in msg.split('='):
			temp = self.subroll(mess[1:])
			if temp is None:
				await self.bot.say("Wow you fucked up the <" + mess[1:] + "> part")
				return
			if mess[0] == '+':
				total += int(temp[0])
			elif mess[0] == '-':
				total -= int(temp[0])
			else:
				await self.bot.say("Someone fucked up that +/- ...")
				return
			totalrolls.append(temp[1])

		await self.bot.say(str(total))
		if self.showrolls[ctx.message.channel.id]:
			await self.bot.say(str(totalrolls))
		return
		
	@commands.command(pass_context=True, no_pm=True)
	async def rollchar(self, ctx):
		'''roll the base stats for a normal D&D 3.5 character'''

		if ctx.message.channel.id not in self.showrolls:
			self.showrolls[ctx.message.channel.id] = False

		results = [0, 0, 0, 0, 0, 0]
		for i in range(6):
			temproll = [0, 0, 0, 0]
			for j in range(4):
				temproll[j] = random.randint(1, 6);
			temproll.sort()
			for j in range(3):
				results[i] += temproll[j+1]
		await self.bot.say(str(results))
