import string
import random
import discord
from discord.ext import commands
import pprint
import commands.database as database
import datetime

HANGMANPICS = ['''```
   +---+
   |   |
       |
       |
       |
       |
 =========```''', '''```
   +---+
   |   |
   O   |
       |
       |
       |
 =========```''', '''```
   +---+
   |   |
   O   |
   |   |
       |
       |
 =========```''', '''```
   +---+
   |   |
   O   |
  /|   |
       |
       |
 =========```''', '''```
   +---+
   |   |
   O   |
  /|\  |
       |
       |
 =========```''', '''```
   +---+
   |   |
   O   |
  /|\  |
  /    |
       |
 =========```''', '''```
   +---+
   |   |
   O   |
  /|\  |
  / \  |
       |
 =========```''']


def random_line(afilename):
	with open(afilename) as afile:
		line = next(afile)
		for num, aline in enumerate(afile):
			if random.randrange(num + 2): continue
			line = aline
		return ''.join([ c for c in line.lower() if c.isalpha()]) # filter out any newlines or weird characters

class hangmanvars:
	def __init__(self, rvf=False):
		self.word = random_line("./wordlist.txt")
		self.display = '.' * len(self.word)
		self.usedletters = set("")
		self.wrong = 0
		self.rvf = rvf
		self.started = True
		self.participants = set("")
		self.winner = ""
		self.startDate = ""
		self.endDate = ""

	def __str__(self):
		retstr = ""
		retstr = retstr + "word: '" + str(self.word) + "'\n"
		retstr = retstr + "display: '" + str(self.display) + "'\n"
		retstr = retstr + "used: '" + str(self.usedletters) + "'\n"
		retstr = retstr + "wrong: '" + str(self.wrong) + "'\n"
		retstr = retstr + "started: '" + str(self.started) + "'\n"
		retstr = retstr + "participants: '" + str(self.participants) + "'\n"
		retstr = retstr + "winner: '" + str(self.winner) + "'\n"
		retstr = retstr + "startdate: '" + str(self.startDate) + "'\n"
		retstr = retstr + "enddate: '" + str(self.endDate) + "'\n"

		return retstr

class HangmanCommands:
	def __init__(self, bot):
		self.bot = bot
		self.vars = {}
	
		
	@commands.group(pass_context=True, invoke_without_command=True, aliases=["hm"])
	async def hangman(self, ctx, msg : str = None):
		'''Starts a game of hangman'''
		if (ctx.message.channel.id not in self.vars) or (not self.vars[ctx.message.channel.id].started):
			rvf = False
			if(msg and ("rvf" in msg.split())):
			    rvf = True
				
			await self.bot.say("Starting a new hangman game! Looking for a word...")
			self.vars[ctx.message.channel.id] = hangmanvars(rvf)
			self.vars[ctx.message.channel.id].startDate = datetime.datetime.now().isoformat()

			await self.bot.say("Found one! Use `!hangman x` or `!hm x` to try the letter x.")
			await self.bot.say("`" + self.vars[ctx.message.channel.id].display + "`")
			if msg == "rvf" or not msg:
				return
			else:
				msg = msg.split()[1]


		if not msg:
			await self.status(ctx)

		#assign to "data" to type 3000 characters less
		data = self.vars[ctx.message.channel.id]
		
		#see if we need to add message sender to participants
		if ctx.message.author not in data.participants:
			#and add them
			data.participants.add(ctx.message.author)

		if (len(msg) != 1) and (msg != "rvf"):
			if msg == data.word:
				await self.bot.say("Correct! The word was " + data.word + "!")
				data.started = False
				data.winner = ctx.message.author
				data.endDate = datetime.datetime.now().isoformat()
				db = database.database()
				db.inserthm(data)
			else:
				await self.bot.say("Incorrect!")
			return

		msg = msg.lower()
		
		if msg not in string.ascii_lowercase:
			await self.bot.say("Try alphabetic characters!")
			return
		if msg in data.usedletters:
			await self.bot.say("That letter has already been said! Try !hangman status for more info.")
			return
		
		if data.rvf and (msg in "aeiou"):
			await self.bot.say("Rad Van Fortuin, you can only guess consonants")
			return

		if msg in data.word:
			data.usedletters.add(msg)
			newdisplay = ""
			# add guessed letter to displayed work
			for id, let in enumerate(data.word):
				if let == msg:
					newdisplay += data.word[id]
				else:
					newdisplay += data.display[id]
			data.display = newdisplay
			# if display and word are equal you've guessed the entire word
			if data.display == data.word:
				await self.bot.say("Correct! The word was " + data.word + "!")
				data.started = False
				data.winner = ctx.message.author
				data.endDate = datetime.datetime.now().isoformat()
				db = database.database()
				db.inserthm(data)
				return
			if data.rvf:
				#see if all consonants have been guessed
				if all([
					( i == j or j in "aeiou")
					for i,j in zip(newdisplay, data.word)
					]):
					await self.bot.say("you've guessed the last consonant!")

		else:
			data.wrong += 1
			data.usedletters.add(msg)
			await self.bot.say(HANGMANPICS[data.wrong - 1])
		await self.bot.say("`" + data.display + "`")
		if data.wrong >= len(HANGMANPICS):
			await self.bot.say("Alas, he's dead. The word was " + data.word + "...")
			data.started = False
	

	@hangman.command(pass_context=True)
	async def start(self, ctx, msg):
		'''Starts a game of hangman'''
		if (ctx.message.channel.id not in self.vars) or (not self.vars[ctx.message.channel.id].started):
			rvf = False
			if("rvf" in msg.split()):
				rvf = True
				
			await self.bot.say("Starting a new hangman game! Looking for a word...")
			self.vars[ctx.message.channel.id] = hangmanvars(rvf)
			self.vars[ctx.message.channel.id].startDate = datetime.datetime.now().isoformat()

			await self.bot.say("Found one! Use `!hangman x` or `!hm x` to try the letter x.")
			await self.bot.say("`" + self.vars[ctx.message.channel.id].display + "`")
		else:
			await self.bot.say("Hangman is already running!")

		
	@hangman.command(pass_context=True)
	async def stop(self, ctx):
		'''Stops the game of hangman'''
		if (ctx.message.channel.id not in self.vars) or (not self.vars[ctx.message.channel.id].started):
			await self.bot.say("Hangman is not running!")
		else:
			self.vars[ctx.message.channel.id].started = False
			await self.bot.say("Too bad, the word was: " + self.vars[ctx.message.channel.id].word + "...")


	@hangman.command(pass_context=True)
	async def status(self, ctx):
		'''Gives info about the current game of hangman'''
		if (ctx.message.channel.id not in self.vars) or (not self.vars[ctx.message.channel.id].started):
			await self.bot.say("Hangman is not running!")
		else:
			if self.vars[ctx.message.channel.id].rvf:
				await self.bot.say("**Chad Rad Van Fortuin**")
			else:
				await self.bot.say("*Hangman easy mode*")
			if self.vars[ctx.message.channel.id].wrong > 0:
				await self.bot.say(HANGMANPICS[self.vars[ctx.message.channel.id].wrong - 1])
			await self.bot.say("`" + self.vars[ctx.message.channel.id].display + "`")
			await self.bot.say("The used letters are: " + str(sorted(list(self.vars[ctx.message.channel.id].usedletters))))

	@hangman.command(pass_context=True)
	async def score(self, ctx, msg : str = None):
		'''Shows people their scores, for bragging purposes mostly'''
		db = database.database()
		if not msg:
			#give score of the current user
			score = db.selectScore(str(ctx.message.author.id))
			if score == -1:
				await self.bot.say("I'm sorry, i don't know you yet. Play some games!")
			else:
				await self.bot.say("<@" + str(ctx.message.author.id) + ">, you've won " + str(score) + " games.")

		else:
			#try to give score for provided user
			print("`" + msg + "`")
			uid = msg[2:-1]
			score = db.selectScore(str(uid))
			if score == -1:
				await self.bot.say("I'm sorry, i don't know them.")
			else:
				await self.bot.say("<@" + str(ctx.message.author.id) + ">, has won " + str(score) + " games.")
	

	@hangman.command(pass_context=True)
	async def top(self, ctx):
		'''Shows the top 5 scores, definitely for bragging'''
		db = database.database()
		topScores = db.selectTopScores()
		output = "The top scores are:"
		for line in topScores:
			output = output + "\n<@" + str(line[1]) + ">: " + str(line[0]) + " points"
		await self.bot.say(output)
