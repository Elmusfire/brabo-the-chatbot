import discord
from discord.ext import commands
import discord.permissions as perm

def make_role_name(msg):
	"""
	add safety measures here
	"""
	while msg.startswith("plays_"): # just to make sure nobody accidentally adds a wrong role
		msg = msg[6:]
	msg = msg.lower()
	return "plays_" + "_".join(msg.split(" "))

class Whosplaying:
	def __init__(self, bot):
		self.bot = bot
	
	
	@commands.command(pass_context=True,aliases=["plays"])
	async def addgame(self, ctx, *, msg : str = None):
		'''Adds a game role to your list of game roles.'''
		rolename = make_role_name(msg)
		msg_author = ctx.message.author
		msg_content = ctx.message.content
		msg_server = ctx.message.server

		temp_role = None
		for role in msg_server.roles:
			if role.name == rolename:
				temp_role = role
				break
		if temp_role is None:
			temp_perm = perm.Permissions.none()
			temp_role = await self.bot.create_role(msg_server,name=rolename, mentionable=True, permissions=temp_perm)
			await self.bot.say("New role \"" + rolename + "\" created.")
		
		await self.bot.add_roles(msg_author, temp_role)
		await self.bot.say("Role \"" + rolename + "\" assigned.")
		
		
		"""
		dir(ctx.message)
		'attachments', 'author', 'call', 'channel', 'channel_mentions', 'clean_content', 'content', 'edited_timestamp', 'embeds', 'id', 'mention_everyone', 'mentions', 'nonce', 'pinned', 'raw_channel_mentions', 'raw_mentions', 'raw_role_mentions', 'reactions', 'role_mentions', 'server', 'system_content', 'timestamp', 'tts', 'type'
		"""
		"""
		dir(ctx.message.server)
		'afk_channel', 'afk_timeout', 'channels', 'created_at', 'default_channel', 'default_role', 'emojis', 'features', 'get_channel', 'get_member', 'get_member_named', 'icon', 'icon_url', 'id', 'large', 'me', 'member_count', 'members', 'mfa_level', 'name', 'owner', 'owner_id', 'region', 'role_hierarchy', 'roles', 'splash', 'splash_url', 'unavailable', 'verification_level', 'voice_client'
		"""
		
		
	@commands.command(pass_context=True)
	async def removegame(self, ctx, *, msg : str = None):
		'''Removes a game role to your list of game roles.'''
		print(msg)
		rolename = make_role_name(msg)
		msg_author = ctx.message.author
		count = 0
		for role in msg_author.roles:
			if role.name == rolename:
				count+=1
				await self.bot.remove_roles(msg_author, role)
		
		maybes = " has" if count == 1 else "s have"
		await self.bot.say(str(count) + " role" + maybes + " been removed.")

		

		

"""
add_roles(member, *roles)
This function is a coroutine.

Gives the specified Member a number of Role s.

You must have the proper permissions to use this function.

The Member object is not directly modified afterwards until the corresponding WebSocket event is received.

Parameters:	
member (Member) – The member to give roles to.
*roles – An argument list of Role s to give the member.
Raises:	
Forbidden – You do not have permissions to add roles.
HTTPException – Adding roles failed.
remove_roles(member, *roles)
This function is a coroutine.

Removes the Role s from the Member.

You must have the proper permissions to use this function.

The Member object is not directly modified afterwards until the corresponding WebSocket event is received.

Parameters:	
member (Member) – The member to revoke roles from.
*roles – An argument list of Role s to revoke the member.
Raises:	
Forbidden – You do not have permissions to revoke roles.
HTTPException – Removing roles failed.
"""