# -*- coding: utf-8 -*-

import asyncio
import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
import os
from discord.ext import commands

class MathCommands:

	def __init__(self, bot):
		self.bot = bot

	@commands.command(pass_context=True, no_pm=False)
	async def math(self, ctx, msg = None):
		"""Compiles LaTeX"""

		formula = ctx.message.clean_content[len("!math"):]
		await self.bot.say("Compiling `" + formula + "`, this might take a while")

		try:
			fig = plt.figure()
			text = fig.text(0, 0, formula)

			# Saving the figure will render the text.
			dpi = 300
			fig.savefig('temp.png', dpi=dpi)
			os.remove("temp.png")

			# Now we can work with text's bounding box.
			bbox = text.get_window_extent()
			width, height = bbox.size / float(dpi) + 0.005
			# Adjust the figure size so it can hold the entire text.
			fig.set_size_inches((width, height))

			# Adjust text's vertical position.
			dy = (bbox.ymin/float(dpi))/height
			text.set_position((0, -dy))

			# Save the adjusted text.
			fig.savefig('formula.png', dpi=dpi)
			with open('formula.png', 'rb') as f:
				await self.bot.send_file(ctx.message.channel, f)
			
		except Exception as e:
			await self.bot.say("Something went wrong, oops.")
