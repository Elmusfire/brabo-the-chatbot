import asyncio
import requests
import time
from discord.ext import commands
import sys
import commands.database as db

class StatsCommands:

	def __init__(self, bot):
		self.bot = bot

	@commands.command(pass_context=True, no_pm=False)
	async def stats(self, ctx, msg = None):
		"""replies with some stats"""

		#await self.bot.say('ctx has: `' + str(dir(ctx)) + '`')
		#await self.bot.say('ctx.message has: `' + str(dir(ctx.message)) + '`')
		print('ctx.message has: `' + str(dir(ctx.message)) + '`')
		
		async for message in self.bot.logs_from(ctx.message.channel, limit=5):
			DB = db.database()
			await self.bot.say(DB.insertStatsLine(message))