import random
import discord
from discord.ext import commands

class roulettevars:
	def __init__(self):
		self.bulletid = random.randint(1, 6)
		self.click = 0

class RussianRoulette:
	def __init__(self, bot):
		self.bot = bot
		self.vars = {}
		
	@commands.group(invoke_without_command=True, pass_context=True)
	async def roulette(self, ctx):
		'''Rusian roulette.'''
		if ctx.message.channel.id not in self.vars:
			self.vars[ctx.message.channel.id] = roulettevars()

		self.vars[ctx.message.channel.id].bulletid -= 1
		if self.vars[ctx.message.channel.id].bulletid > 0:
			await self.bot.say("*click*")
			self.vars[ctx.message.channel.id].click += 1
		else:
			await self.bot.say("**BANG!**")
			await self.bot.say("Reloading...")
			self.vars[ctx.message.channel.id] = roulettevars()
		if self.vars[ctx.message.channel.id].click >= 5:
			await self.bot.say("Clicked 5 times, spinning the chamber...")
			self.vars[ctx.message.channel.id] = roulettevars()
		
	@roulette.command(pass_context=True)
	async def spin(self, ctx):
		'''Spin the chamber!'''
		if ctx.message.channel.id not in self.vars:
			self.vars[ctx.message.channel.id] = roulettevars()
		await self.bot.say("Spinning the chamber...")
		self.vars[ctx.message.channel.id] = roulettevars()
		
	@roulette.command(pass_context=True)
	async def reload(self, ctx):
		'''Reload the gun and spin the chamber.'''
		if ctx.message.channel.id not in self.vars:
			self.vars[ctx.message.channel.id] = roulettevars()
		await self.bot.say("Reloading...")
		self.vars[ctx.message.channel.id] = roulettevars()
